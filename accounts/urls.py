from django.urls import path, include
from accounts.views import base_view, upload_file



urlpatterns = [
        path("upload/", upload_file, name='upload_file'), 
        path("", base_view, name='home'),

]
