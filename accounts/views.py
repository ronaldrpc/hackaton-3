import pathlib
import pandas as pd
from django.shortcuts import render
from importer.factory import *
from importer.tasks import *
from django.contrib.auth.decorators import login_required




# Create your views here.
from importer.models import UploadFile

@login_required
def upload_file(request):
    msg = ''
    importer = None
    profiles = Profile.objects.all()
    if request.method == "POST":
        file_title = request.POST["fileTitle"]
        upload_files = request.FILES['uploadedFile']
        extension = pathlib.Path(upload_files.name).suffix

        if extension == ".csv":
            document = UploadFile(title=file_title, uploadedFile=upload_files)
            document.save()
            import_csv_data.delay(document.uploadedFile.name)
    else:
        profiles = Profile.objects.all()
        msg = ""
    return render(request, "upload_datatable.html", context={'profiles': profiles, 'msg': msg})



def base_view(request):
    return render(request, "index.html")
