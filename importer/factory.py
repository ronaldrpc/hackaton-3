from importer.importers import *


class ImporterFactory(ABC):
    @abstractmethod
    def get_importer(self, path):
        pass


class CsvImporterFactory(ImporterFactory):
    def get_importer(self, path) -> Importer:
        return CsvImporter(path)



