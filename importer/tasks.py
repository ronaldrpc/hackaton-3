from django.core.mail import send_mail
from celery import shared_task
from importer.importers import CsvImporter
from importer.models import UploadLog
from profiles.models import Profile


def save_log(log):
    UploadLog.objects.create(
        description=log
    )


def save_profile(path, file_index, profile_data, logs):
    try:
        Profile.objects.create(**profile_data)
    except Exception:
        log_error = f"No se guardó el registro de la fila {file_index + 2} del archivo: '{path.split('uploaded/')[1]}'"
        save_log(log_error)
        logs.append(log_error)


def prepare_csv_data(path, file_index, header, row, logs):
    data = {}
    for i in range(len(row)):
        data[header[i]] = row[i]
    save_profile(path, file_index, data, logs)


@shared_task
def import_csv_data(path):
    importer = CsvImporter(path)
    csv_header, csv_values = importer.get_data()
    logs = []
    for file_index, value in enumerate(csv_values):
        prepare_csv_data(path, file_index, csv_header, value, logs)

    # Notify success task end
    send_email_notification('Import file task finished', ['Imported task was successfully completed'])
    # Notify log errors
    send_email_notification('Errors during import file task', logs)


def send_email_notification(subject, msg):
    message = '\n'.join(msg)

    send_mail(
        subject,
        message,
        'from@example.com',
        ['payares03@gmail.com'],
        fail_silently=False)


