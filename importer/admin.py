from django.contrib import admin
from importer.models import UploadFile, UploadLog


# Register your models here.


@admin.register(UploadFile)
class UploadFileAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'uploadedFile', 'datetime_uploaded']


@admin.register(UploadLog)
class UploadLogAdmin(admin.ModelAdmin):
    list_display = ['id', 'description', 'datetime']


