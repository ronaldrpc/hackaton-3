import pathlib
import pandas as pd
from django.shortcuts import render
from importer.factory import *
from importer.tasks import *


# Create your views here.
from importer.models import UploadFile


def upload_file(request):
    msg = ''
    documents = UploadFile.objects.all()
    if request.method == "POST":
        file_title = request.POST["fileTitle"]
        upload_files = request.FILES['uploadedFile']
        extension = pathlib.Path(upload_files.name).suffix

        if extension == ".csv":
            document = UploadFile(title=file_title, uploadedFile=upload_files)
            document.save()
            import_csv_data.delay(document.uploadedFile.name)
    else:
        documents = UploadFile.objects.all()
        msg = ""
    return render(request, "upload_file.html", context={'files': documents, 'msg': msg})
