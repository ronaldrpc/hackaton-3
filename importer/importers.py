import csv
import pandas as pd
from abc import ABC, abstractmethod


class Importer(ABC):
    @abstractmethod
    def get_data(self):
        pass


class CsvImporter(Importer):

    def __init__(self, path):
        self.path = path

    def get_data(self):
        BASE = 'media/'
        df = pd.read_csv(BASE + self.path)
        header = df.columns.tolist()
        rows = df.values
        return header, rows

    def read_file_with_csv(self):
        BASE = 'media/'

        rows = []
        with open(BASE + self.path, 'r') as file:
            csv_reader = csv.reader(file)
            for row in csv_reader:
                if row != ['', '', '', '', '', '', '', '', '', '']:
                    header = row
                    break

            for row in csv_reader:
                rows.append(row)

        return header, rows


# doc_type=row[0],
#         document_number=row[1],
#         first_name=row[2],
#         last_name=row[3],
#         phone=row[4],
#         address=row[5],
#         email=row[6],
#         profession=row[7],
#         profession_grade=row[8],
#         years_experience=row[9],
