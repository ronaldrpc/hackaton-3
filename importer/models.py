from django.db import models


# Create your models here.
class UploadFile(models.Model):
    title = models.CharField(max_length=100)
    uploadedFile = models.FileField(upload_to='uploaded')
    datetime_uploaded = models.DateTimeField(auto_now_add=True)


class UploadLog(models.Model):
    description = models.TextField()
    datetime = models.DateTimeField(auto_now_add=True)



