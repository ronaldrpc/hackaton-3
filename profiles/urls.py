from django.urls import path, include
from . import views
from django.contrib.auth.decorators import login_required


urlpatterns = [
    path('detail/<int:pk>/', login_required(views.ProfileDetailView.as_view()), name='detail'),
]
