# Generated by Django 4.0.4 on 2022-05-07 16:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('doc_type', models.CharField(default=None, max_length=50)),
                ('document_number', models.CharField(default=None, max_length=50)),
                ('first_name', models.CharField(default=None, max_length=50)),
                ('last_name', models.CharField(default=None, max_length=50)),
                ('phone', models.CharField(default=None, max_length=50)),
                ('address', models.CharField(default=None, max_length=150)),
                ('email', models.EmailField(max_length=254)),
                ('profession', models.CharField(default=None, max_length=50)),
                ('profession_grade', models.CharField(default=None, max_length=50)),
                ('years_experience', models.FloatField()),
            ],
        ),
    ]
