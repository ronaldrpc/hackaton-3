from django.shortcuts import render

from django.views.generic.detail import DetailView


from profiles.models import Profile


class ProfileDetailView(DetailView):
    model = Profile
    template_name = 'detailhojadevida.html'
    context_object_name='profile'


