from django.db import models, IntegrityError
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.core.validators import validate_email, EmailValidator


# Create your models here.

class Profile(models.Model):
    """
    Model to storage the profile information contained in the uploaded file
    """
    doc_type = models.CharField(max_length=50, blank=False, null=False, default=None)
    document_number = models.CharField(max_length=50, blank=False, null=False, default=None)
    first_name = models.CharField(max_length=50, blank=False, null=False, default=None)
    last_name = models.CharField(max_length=50, blank=False, null=False, default=None)
    phone = models.CharField(max_length=50, blank=False, null=False, default=None)
    address = models.CharField(max_length=150, blank=False, null=False, default=None)
    profession = models.CharField(max_length=50, blank=False, null=False, default=None)
    profession_grade = models.CharField(max_length=50, blank=False, null=False, default=None)
    years_experience = models.FloatField()
    email = models.EmailField(
        _('email address'),
        null=False,
        blank=False,
        default=None,
        validators=[
            validate_email,
        ],
        unique=True,
    )

    def __str__(self):
        return f'{self.first_name} {self.last_name}, {self.profession_grade} - {self.profession} con {self.years_experience} años de experiencia.'

    def save(self, *args, **kwargs):
        """
        Edit default save to implement email validator
        """
        try:
            validate_email(self.email)
        except ValidationError as error:
            raise IntegrityError(error.message)
        super().save(*args, **kwargs)
