from django.db import IntegrityError
from django.test import TransactionTestCase, TestCase
from profiles.models import Profile



class TestModels(TransactionTestCase):
    """
    Test for Profile model
    """

    valid_data = {'doc_type': 'PA',
                  'document_number': 'PHA11122233344',
                  'first_name': 'Test Name',
                  'last_name': 'Test Last Name',
                  'phone': '+57 321 4567890',
                  'address': 'Cartagena, Test #54-78',
                  'email': 'test@email.com',
                  'profession': 'Developer',
                  'profession_grade': 'Technology',
                  'years_experience': 2.5, }

    def test_required_doc_type(self):
        del self.valid_data['doc_type']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_document_number(self):
        del self.valid_data['document_number']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_first_name(self):
        del self.valid_data['first_name']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_last_name(self):
        del self.valid_data['last_name']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_phone(self):
        del self.valid_data['phone']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_address(self):
        del self.valid_data['address']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_email(self):
        del self.valid_data['email']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_profession(self):
        del self.valid_data['profession']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_profession_grade(self):
        del self.valid_data['profession_grade']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_required_years_experience(self):
        del self.valid_data['years_experience']
        with self.assertRaises(IntegrityError):
            Profile.objects.create(**self.valid_data)

    def test_invalid_field_values(self):
        invalid_emails = {
            'testgemail.com',
            '@mail.com',
            'gmail',
            '',
            '.com',
            'com',
        }

        for value in invalid_emails:
            self.valid_data['email'] = value
            with self.assertRaises(IntegrityError):
                Profile.objects.create(**self.valid_data)



